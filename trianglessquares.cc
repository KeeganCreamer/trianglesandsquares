#include <iostream>
int main() 
{
	std::cout << "Please enter your desired shape(Triangle or Square): " << std::endl;
	std::string desiredShape;
	std::cin >> desiredShape;
	std::cout << "And then the size(length of one side): " << std::endl;
	int desiredSize;
	std::cin >> desiredSize;
	int count = 0;
	std::cout << std::endl;
		if( desiredShape=="Triangle"){
			std::string trianglePrintL = "";
			while( count < desiredSize){
				trianglePrintL = trianglePrintL + "*" ;
				std::cout << trianglePrintL << std::endl;
				count ++;
			}
		}
		else if( desiredShape=="Square"){
			std::string squarePrintL = "";
			while(count< desiredSize ){
				squarePrintL += "*";
				count ++;
				if(count == desiredSize){
					while(count < (desiredSize *2)){
						std::cout << squarePrintL << std::endl;
						count ++;
					}
				}
			}
		}
}